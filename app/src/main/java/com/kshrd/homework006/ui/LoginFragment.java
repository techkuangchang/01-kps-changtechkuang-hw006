package com.kshrd.homework006.ui;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kshrd.homework006.NavigationDrawerActivity;
import com.kshrd.homework006.R;

public class LoginFragment extends Fragment {

    private EditText mEmail, mPassword;
    private ImageView mLogin, mRegister;

    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener authStateListener;

    static final String TAG = "UserLogin";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_login, container, false);
        mAuth = FirebaseAuth.getInstance();

        mEmail = view.findViewById(R.id.email_username);
        mPassword = view.findViewById(R.id.password);
        mLogin = view.findViewById(R.id.btnLogin);
        mRegister = view.findViewById(R.id.reg_btn);

        //************************ Handle login
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragment();
            }
        });

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if(firebaseUser != null) {
                    userInfo(firebaseUser);
                }
                else {
                    userInfo(null);
                }
            }
        };

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authStateListener);
    }

    private void Login() {
        String email, password;

        email = mEmail.getText().toString().trim();
        password = mPassword.getText().toString().trim();

        if(email.isEmpty()) {
            mEmail.setError("Please enter your email.");
            mEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmail.setError("Please provide your email.");
            mEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            mPassword.setError("Please enter your password.");
            mPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            mPassword.setError("Password must be 6 characters or more than.");
            mPassword.requestFocus();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser firebaseUser = mAuth.getCurrentUser();
                    userInfo(firebaseUser);
                    Toast toast = Toast.makeText(getActivity(), "Login Successful", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL , 0, 0);
                    toast.show();

                    Intent intentHome = new Intent(getActivity(), NavigationDrawerActivity.class);
                    startActivity(intentHome);
                }
                else{
                    Toast toast = Toast.makeText(getActivity(), "Failed to login ! Wrong Email or Password.", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL , 0, 0);
                    toast.show();;
                }
            }
        });
    }

    public void userInfo(FirebaseUser firebaseUser) {

//        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if(firebaseUser != null) {
            DatabaseReference mReference = FirebaseDatabase.getInstance().getReference().child("User");
            mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String getEmail = snapshot.child(firebaseUser.getUid()).child("email").getValue(String.class);
                    String getUsername = snapshot.child(firebaseUser.getUid()).child("username").getValue(String.class);
                    Log.d("User","Email -> "+getEmail);
                    Log.d("User","Username -> "+getUsername);
                    Intent intent = new Intent(getActivity(),NavigationDrawerActivity.class);
                    intent.putExtra("EmailUser",getEmail);
                    intent.putExtra("UserName",getUsername);
                    startActivity(intent);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.e("Data is null",error.getMessage());
                }
            });
        }
        else {

        }
    }

    private void RegisterFragment(){
        Fragment mFragmentRegister = new RegisterFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.login_screen, mFragmentRegister);
        fragmentTransaction.commit();
    }

}
