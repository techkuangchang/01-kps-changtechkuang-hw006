package com.kshrd.homework006.ui;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kshrd.homework006.LoginScreenActivity;
import com.kshrd.homework006.NavigationDrawerActivity;
import com.kshrd.homework006.R;
import com.kshrd.homework006.model.UserModel;

public class RegisterFragment extends Fragment {

    private EditText mEmail, mPassword, mUsername;
    private ImageView mEnter, mLogin;
    FirebaseDatabase database;
    DatabaseReference myRef;

    private FirebaseAuth mAuth;
    private String userId;

    static final String TAG = "Register";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_register, container, false);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users");

        mEmail = view.findViewById(R.id.email_username_reg);
        mPassword = view.findViewById(R.id.password_reg);
        mEnter = view.findViewById(R.id.btn_login_reg);
        mLogin = view.findViewById(R.id.reg_btn_reg);
        mUsername = view.findViewById(R.id.username_reg);

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginFragment();
            }
        });

        mEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register();
            }
        });

        return view;
    }

    private void Register() {
        String email, password, username;

        username = mUsername.getText().toString().trim();
        email = mEmail.getText().toString().trim();
        password = mPassword.getText().toString().trim();

        if(username.isEmpty()) {
            mUsername.setError("Please enter your username");
            mUsername.requestFocus();
            return;
        }

        if(email.isEmpty()) {
            mEmail.setError("Please enter your email.");
            mEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmail.setError("Please provide your email.");
            mEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            mPassword.setError("Please enter your password.");
            mPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            mPassword.setError("Password must be 6 characters or more than.");
            mPassword.requestFocus();
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    UserModel userModel = new UserModel(username, email, password);
                    FirebaseDatabase.getInstance().getReference("User")
                            .child(FirebaseAuth.getInstance().getUid())
                            .setValue(userModel);
                    Log.d(TAG,"User Model"+userModel);

                    Toast toast = Toast.makeText(getActivity(), "Register has been successful", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL , 0, 0);
                    toast.show();

                    Intent intent = new Intent(getActivity(), LoginScreenActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast toast = Toast.makeText(getActivity(), "Failed to register !", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL , 0, 0);
                    toast.show();
                }
            }
        });

    }

    private void updateUser(String email, String password, String username) {
        if (!TextUtils.isEmpty(username))
            myRef.child(userId).child("name").setValue(username);

        if (!TextUtils.isEmpty(email))
            myRef.child(userId).child("email").setValue(email);

        if (!TextUtils.isEmpty(password))
            myRef.child(userId).child("password").setValue(password);
    }

    private void UserListener() {
        myRef.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserModel user =snapshot.getValue(UserModel.class);

                if(user == null) {
                    return;
                }

                String username = user.getUsername();
                Intent intent = new Intent(getActivity(), NavigationDrawerActivity.class);
                intent.putExtra("UserName",username);
                startActivity(intent);

                Log.d(TAG,"Value is : "+intent);
                Log.d(TAG,"Value is : "+user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    //**************** Checking internet permission
    private boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiConn = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if(networkInfo !=null && networkInfo.isConnected() || wifiConn != null && wifiConn.isConnected()){
            return true;
        }else {
            return false;
        }
    }

    private void LoginFragment(){
        Fragment mFragmentLogin = new LoginFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_screen, mFragmentLogin);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
